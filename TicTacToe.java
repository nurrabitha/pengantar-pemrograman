package Tugas2;
import java.util.*;
public class TicTacToe {
	
		static Scanner scn=new Scanner(System.in);
		static void play(){
				int number=-1,player=1,option;
				char[][]tictactoe=new char[3][3];
				for(int row=0;row<3;row++){
					for(int column=0;column<3;column++){
						tictactoe[row][column]=' ';
					}
				}
				do{
					int choose2 = -1;
					do{
						String choose="a";
						for(int row=0;row<3;row++){
							for(int column=0;column<3;column++){
								tictactoe[row][column]=' ';
							}
						}
						System.out.print("Pilih Tanda ( X / O ) : ");
						
						try{
							choose=scn.nextLine();
							if(choose.contentEquals("X") == false && choose.contentEquals("O") == false){
								throw new InputMismatchException();
							}
						}catch(InputMismatchException e){
							System.out.println("-----------------------------------");
							System.out.println("Masukkan huruf 'X' atau 'O' ");
						}
						
							if(choose.contentEquals("X")){
								number=1;
								break;
							}
							if(choose.contentEquals("O")){
								number=0;
								break;
							}
							
					}while(true);
					
					while(true){
						int content=0;
						System.out.println("===================================");
						System.out.println("\tGiliran Pemain Ke-"+player);
						System.out.println();
						System.out.println("\t  "+tictactoe[0][0]+" |   "+tictactoe[0][1]+"   | "+tictactoe[0][2]);
						System.out.println("\t____|_______|____");
						System.out.println("\t  "+tictactoe[1][0]+" |   "+tictactoe[1][1]+"   | "+tictactoe[1][2]);
						System.out.println("\t____|_______|____");
						System.out.println("\t  "+tictactoe[2][0]+" |   "+tictactoe[2][1]+"   | "+tictactoe[2][2]);
						System.out.println("\t    |       |    ");
						System.out.println("\n");
						
					
						
						if(		tictactoe[0][0]!=' ' && tictactoe[0][0]==tictactoe[0][1] && tictactoe[0][0]==tictactoe[0][2]||
								tictactoe[1][0]!=' ' && tictactoe[1][0]==tictactoe[1][1] && tictactoe[1][0]==tictactoe[1][2]||
								tictactoe[2][0]!=' ' && tictactoe[2][0]==tictactoe[2][1] && tictactoe[2][0]==tictactoe[2][2]||
								tictactoe[0][0]!=' ' && tictactoe[0][0]==tictactoe[1][1] && tictactoe[0][0]==tictactoe[2][2]||
								tictactoe[0][2]!=' ' && tictactoe[0][2]==tictactoe[1][1] && tictactoe[0][2]==tictactoe[2][0]||
								tictactoe[0][0]!=' ' && tictactoe[0][0]==tictactoe[1][0] && tictactoe[0][0]==tictactoe[2][0]||
								tictactoe[0][1]!=' ' && tictactoe[0][1]==tictactoe[1][1] && tictactoe[0][1]==tictactoe[2][1]||
								tictactoe[0][2]!=' ' && tictactoe[0][2]==tictactoe[1][2] && tictactoe[0][2]==tictactoe[2][2])
						{
							if(player==1)player++;
							else if(player==2)player--;
							System.out.println("PERMAINAN BERAKHIR, PEMAIN KE-"+player+" MENANG");
							
							do{
								try{
									System.out.print("Tekan 1 untuk Bermain Lagi dan 0 untuk keluar : ");
									choose2=scn.nextInt();
									
									if((choose2==1) == false && (choose2==0) == false){
										throw new InputMismatchException();
									}
								}
								catch(InputMismatchException e){
									System.out.println("------------------------------");
									System.out.println("Masukkan Angka 1 atau 0");
									scn.nextLine();
									continue;
								}break;
							}while(true);
							break;
						}
						
						
						for(int row=0;row<3;row++){
							for(int column=0;column<3;column++){
								if(tictactoe[row][column]!=' '){
									content++;
								}
							}
						}if (content==9){
							System.out.println("PERMAINAN BERAKHIR, KEDUA PEMAIN SERI");
							do{
								try{
									System.out.print("Tekan 1 untuk Bermain Lagi dan 0 untuk keluar : ");
									choose2=scn.nextInt();
									
									if((choose2==1) == false && (choose2==0) == false){
										throw new InputMismatchException();
									}
								}
								catch(InputMismatchException e){
									System.out.println("------------------------------");
									System.out.println("Masukkan Angka 1 atau 0");
									scn.nextLine();
									continue;
								}break;
							}while(true);
							break;
						}
						
						
						System.out.println("Masukkan Angka yang Tersedia Untuk Bermain :");
						if(tictactoe[0][0]==' '){
							System.out.println("1. Row 1 Columns 1");
						}
						if(tictactoe[0][1]==' '){
							System.out.println("2. Row 1 Columns 2");
						}
						if(tictactoe[0][2]==' '){
							System.out.println("3. Row 1 Columns 3");
						}
						if(tictactoe[1][0]==' '){
							System.out.println("4. Row 2 Columns 1");
						}
						if(tictactoe[1][1]==' '){
							System.out.println("5. Row 2 Columns 2");
						}
						if(tictactoe[1][2]==' '){
							System.out.println("6. Row 2 Columns 3");
						}
						if(tictactoe[2][0]==' '){
							System.out.println("7. Row 3 Columns 1");
						}
						if(tictactoe[2][1]==' '){
							System.out.println("8. Row 3 Columns 2");
						}
						if(tictactoe[2][2]==' '){
							System.out.println("9. Row 3 Columns 3");
						}
						do{
							
							System.out.print(">> ");
							option=-1;
							try{
								option=scn.nextInt();
								break;
							}catch(InputMismatchException e){
								System.out.println("Masukkan Pilihan yang Tersedia");
								scn.nextLine();
								option=-1;
								continue;
							}
						}while(true);
						
						switch(option){
						case 1:
							if(tictactoe[0][0]==' ' && option==1){
								if(number==1){
									tictactoe[0][0]='X';
									number--;
								}
								else if(number==0){
									tictactoe[0][0]='o';
									number++;
								}
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[0][0]!=' ' && option==1){
								System.out.println("Masukkan Pilihan di Ruang yang Kosong");
							}break;
						case 2:
							if(tictactoe[0][1]==' ' && option==2){
								if(number==1){
									tictactoe[0][1]='X';
									number--;
								}
								else if(number==0){
									tictactoe[0][1]='o';
									number++;
								}
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[0][1]!=' ' && option==2){
								System.out.println("Masukkan Pilihan di Ruang yang Kosong");
							}break;
						case 3:
							if(tictactoe[0][2]==' ' && option==3){
								if(number==1){
									tictactoe[0][2]='X';
									number--;
								}
								else if(number==0){
									tictactoe[0][2]='O';
									number++;
								}	
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[0][2]!=' ' && option==3){
								System.out.println("Masukkan Pilihan di Ruang yang Kosong");
							}
						case 4:
							if(tictactoe[1][0]==' ' && option==4){
								if(number==1){
									tictactoe[1][0]='X';
									number--;
								}
								else if(number==0){
									tictactoe[1][0]='O';
									number++;
								}	
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[1][0]!=' ' && option==4){
								System.out.println("Masukkan Pilihan di Ruang yang Kosong");
							}break;
						case 5:
							if(tictactoe[1][1]==' ' && option==5){
								if(number==1){
									tictactoe[1][1]='X';
									number--;
								}
								else if(number==0){
									tictactoe[1][1]='O';
									number++;
								}	
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[1][1]!=' ' && option==5){
								System.out.println("Masukkan Pilihan di Ruang yang Kosong");
							}break;
						case 6:
							if(tictactoe[1][2]==' ' && option==6){
								if(number==1){
									tictactoe[1][2]='X';
									number--;
								}
								else if(number==0){
									tictactoe[1][2]='O';
									number++;
								}	
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[1][2]!=' ' && option==6){
								System.out.println("Masukkan Pilihan di Ruang yang Kosong");
							}break;
						case 7:
							if(tictactoe[2][0]==' ' && option==7){
								if(number==1){
									tictactoe[2][0]='X';
									number--;
								}
								else if(number==0){
									tictactoe[2][0]='O';
									number++;
								}	
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[2][0]!=' ' && option==7){
								System.out.println("Masukkan Pilihan di Ruang yang Kosong");
							}break;
						case 8:
							if(tictactoe[2][1]==' ' && option==8){
								if(number==1){
									tictactoe[2][1]='X';
									number--;
								}
								else if(number==0){
									tictactoe[2][1]='O';
									number++;
								}
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[2][1]!=' ' && option==8){
								System.out.println("Masukkan Pilihan di Ruang yang Kosong");
							}break;
						case 9:
							if(tictactoe[2][2]==' ' && option==9){
								if(number==1){
									tictactoe[2][2]='X';
									number--;
								}
								else if(number==0){
									tictactoe[2][2]='O';
									number++;
								}	
									if(player==1)player++;
									else if(player==2)player--;
							}
							else if(tictactoe[2][2]!=' ' && option==9){
								System.out.println("Masukkan Pilihan di Ruang yang Kosong");
							}break;
						default:
							System.out.println("Masukkan Pilihan Yang Sesuai");
							break;
						}				
					}
					
					if(choose2 == 1 ){
						continue;
					}
					else if(choose2 == 0 ){
						break;
					}
					
				}while(true);
		}
		
		public static void main(String[] args) {
			do{
				int option=-1;
					
				System.out.println();
				System.out.println("***********TIC TAC TOE***********");
				System.out.println("1. Play");
				System.out.println("0. Exit");
				System.out.print("Masukkan Pilihan : ");
				
				try{
					option=scn.nextInt();
				}catch(InputMismatchException e){
					System.out.println("Masukkan Angka 1 atau 0");
				}
				switch(option){
				case 1:
					scn.nextLine();
					play();
					break;
				case 0:
					System.exit(0);
				default:
				 	System.out.println();
					System.out.println("Masukkan Angka 1 atau 0");
					break;
				}
			
			}while(true);
		}
	}
