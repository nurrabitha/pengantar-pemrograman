package Tugas2;
import java.util.*;
public class no3 {

		static Scanner scn=new Scanner(System.in);
		static int choose;
		static void MainMenu(){
			while(true){
				int option;
				System.out.println("\t\t   Matriks");
				System.out.println("===========================================");			
				System.out.println("1. Penjumlahan");
				System.out.println("2. Pengurangan");
				System.out.println("3. Perkalian");
				System.out.println("0. Keluar");
				System.out.println("===========================================");	
				System.out.print("Masukkan Pilihan : ");
				do{
					try{
						option=scn.nextInt();
						if(option!= 1 && option!= 2 && option!= 3  && option!= 0 ){
							System.out.println("Masukkan pilihan yang tersedia");
							continue;
						}
						break;
					}catch(InputMismatchException e){
						System.out.println("Masukkan pilihan yang tersedia");
						scn.nextLine();
						continue;
					}
				}while(true);
				if(option== 0){
					System.exit(0);
				}
				switch(option){
					case 1:
						penjumlahan();
						break;
					case 2:
						pengurangan();
						break;
					case 3:
						perkalian();
						break;	
				}	
			}
		}
		
		static void penjumlahan(){
			int baris,kolom;
			do{
				System.out.println("\t\t  Penjumlahan");
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				do{	
					try{				
						System.out.print("Masukkan jumlah baris matriks : ");
						baris=scn.nextInt();
							if (baris <1){
								throw new InputMismatchException();
							}
						System.out.print("Masukkan jumlah kolom matriks : ");
						kolom=scn.nextInt();
							if (kolom <1){
								throw new InputMismatchException();
							}
						break;
					}catch(InputMismatchException e){
						System.out.println("Masukkan angka lebih besar dari 0");
						scn.nextLine();
						continue;
					}
				}while(true);			
				double[][]matriks1= new double[baris][kolom];
				double[][]matriks2= new double[baris][kolom];
				double[][]hasil=new double[baris][kolom];
				System.out.println("\t\t Matriks A");
				do{
					try{
						for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
							for(int indekskolom=0; indekskolom<kolom;indekskolom++){
								System.out.print("Masukkan nilai baris "+(indeksbaris+1)+" dan Kolom "+(indekskolom+1)+" : ");
								matriks1[indeksbaris][indekskolom]=scn.nextDouble();
							}
						}
						break;
					}catch(InputMismatchException e){
						System.out.println("Masukkan bilangan real");
						scn.nextLine();
						continue;
					}	
				}while(true);
				System.out.println();
				for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
					for(int indekskolom=0; indekskolom<kolom;indekskolom++){
						System.out.printf("%.2f  ",matriks1[indeksbaris][indekskolom]);
					}System.out.println();
				}
				System.out.println();
				System.out.println("\t\t Matriks B");
				do{
					try{
						for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
							for(int indekskolom=0; indekskolom<kolom;indekskolom++){
								System.out.println("Masukkan nilai baris"+(indeksbaris+1)+" dan Kolom "+(indekskolom+1));
								System.out.print(">> ");
								matriks2[indeksbaris][indekskolom]=scn.nextDouble();
								hasil[indeksbaris][indekskolom]=matriks1[indeksbaris][indekskolom] + matriks2[indeksbaris][indekskolom];
							}
						}
						break;
					}catch(InputMismatchException e){
						System.out.println("Masukkan bilangan real");
						scn.nextLine();
						continue;
					}	
				}while(true);
				
				System.out.println();
				for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
					for(int indekskolom=0; indekskolom<kolom;indekskolom++){
						
						System.out.printf("%.2f  ",matriks2[indeksbaris][indekskolom]);
					}System.out.println();
				}			
				System.out.println("\n");
				System.out.println("Hasil dari penjumlahan kedua matriks adalah : ");
				System.out.println();
				for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
					for(int indekskolom=0; indekskolom<kolom;indekskolom++){
						
						System.out.printf("%.2f  ",hasil[indeksbaris][indekskolom]);
					}System.out.println();
				}
				if(baris != kolom){
					System.out.println("Matriks tersebut tidak memiliki determinan");
				}	
				else{
					
					if(baris==2 && kolom==2){
						if(((hasil[0][0]*hasil[1][1])-(hasil[0][1]*hasil[1][0]))!=0){
							System.out.println("Determinan dari matriks tersebut adalah "+((hasil[0][0]*hasil[1][1])-(hasil[0][1]*hasil[1][0])));
						}
						else{
							System.out.println("Matriks tersebut merupakan matriks pencerminan");
						}
					}
					else if(baris==3 && kolom==3){
						if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
								-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
								+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
							System.out.println("Determinan dari matriks tersebut adalah "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
									-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
									+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
						}
						else{
							System.out.println("Matriks tersebut merupakan matriks pencerminan");
						}
					}
				}	
				System.out.println();
				scn.nextInt();
				do{
					System.out.println("Tekan 1 Untuk ulangi dan 0 untuk keluar");
					choose=scn.nextInt();
					if (choose==0){
						 break;
					 }
					 else  if (choose==1){
						 break;
					 }
					 else{
						 System.out.println("Masukkan angka 1 atau 0");
						 continue;
					 }
				}while(true);
				 if (choose==0){
					 break;
				 }
				 else  if (choose==1){
					 continue;
				 }
			}while(true);		
		}
		
		static void pengurangan(){
			int baris,kolom;
			do{
				System.out.println("\t\t  Pengurangan");
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");	
				do{	
					try{
						
						System.out.print("Masukkan jumlah baris matriks : ");
						baris=scn.nextInt();
							if (baris <1){
								throw new InputMismatchException();
							}
						System.out.print("Masukkan jumlah kolom matriks : ");
						kolom=scn.nextInt();
							if (kolom <1){
								throw new InputMismatchException();
							}
						break;
					}catch(InputMismatchException e){
						System.out.println("Masukkan angka lebih besar dari 0");
						scn.nextLine();
						continue;
					}
				}while(true);	
				double[][]matriks1= new double[baris][kolom];
				double[][]matriks2= new double[baris][kolom];
				double[][]hasil=new double[baris][kolom];
				System.out.println("\t\t Matriks A");
				do{
					try{
						for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
							for(int indekskolom=0; indekskolom<kolom;indekskolom++){
								System.out.println("Masukkan nilai baris "+(indeksbaris+1)+" dan Kolom "+(indekskolom+1));
								System.out.print(">> ");
								matriks1[indeksbaris][indekskolom]=scn.nextDouble();
							}
						}
						break;
					}catch(InputMismatchException e){
						System.out.println("Masukkan bilangan real");
						scn.nextLine();
						continue;
					}	
				}while(true);			
				System.out.println();
				for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
					for(int indekskolom=0; indekskolom<kolom;indekskolom++){
						
						System.out.printf("%.2f  ",matriks1[indeksbaris][indekskolom]);
					}System.out.println();
				}	
				System.out.println();		
				System.out.println("\t\t Matriks B");
				do{
					try{
						for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
							for(int indekskolom=0; indekskolom<kolom;indekskolom++){
								System.out.println("Masukkan nilai baris "+(indeksbaris+1)+" dan Kolom "+(indekskolom+1));
								System.out.print(">> ");
								matriks2[indeksbaris][indekskolom]=scn.nextDouble();
								hasil[indeksbaris][indekskolom]= matriks1[indeksbaris][indekskolom] - matriks2[indeksbaris][indekskolom];
							}
						}
						break;
					}catch(InputMismatchException e){
						System.out.println("Masukkan bilangan real");
						scn.nextLine();
						continue;
					}	
				}while(true);		
				System.out.println();
				for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
					for(int indekskolom=0; indekskolom<kolom;indekskolom++){
						
						System.out.printf("%.2f  ",matriks2[indeksbaris][indekskolom]);
					}System.out.println();
				}		
				System.out.println("\n");
				System.out.println("Hasil pengurangan kedua matriks tersebut adalah : ");
				System.out.println();	
				for(int indeksbaris=0; indeksbaris<baris;indeksbaris++){
					for(int indekskolom=0; indekskolom<kolom;indekskolom++){
						
						System.out.printf("%.2f  ",hasil[indeksbaris][indekskolom]);
					}System.out.println();
				}
				if(baris != kolom){
					System.out.println("Matriks tersebut tidak memiliki determinan");
				}	
				else{
					
					if(baris==2 && kolom==2){
						if(((hasil[0][0]*hasil[1][1])-(hasil[0][1]*hasil[1][0]))!=0){
							System.out.println("Determinan dari matriks tersebut adalah "+((hasil[0][0]*hasil[1][1])-(hasil[0][1]*hasil[1][0])));
						}
						else{
							System.out.println("Matriks tersebut merupakan matriks pencerminan");
						}
					}
					else if(baris==3 && kolom==3){
						if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
								-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
								+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
							System.out.println("Determinan dari matriks tersebut adalah "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
									-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
									+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
						}
						else{
							System.out.println("Matriks tersebut merupakan matriks pencerminan");
						}
					}
				}
				System.out.println();
				scn.nextLine();
				do{
					System.out.println("Tekan 1 untuk ulangi dan 0 untuk keluar");
					choose=scn.nextInt();
					if (choose==0){
						 break;
					 }
					 else  if (choose==1){
						 break;
					 }
					 else{
						 System.out.println("Masukkan angka 1 atau 0");
						 continue;
					 }
				}while(true);
				
					 if (choose==0){
						 break;
					 }
					 else  if (choose==1){
						 continue;
					 }		
			}while(true);	
		}
		
		static void perkalian(){	
			while(true){
				 int matriksMultiTotal=0,choose=0,baris1,kolom1,baris2,kolom2;
			 do{
				 do{
					 try{
						 System.out.print("Jumlah baris matriks A: ");
						 baris1=scn.nextInt();
						 System.out.print("Jumlah kolom matriks A: ");
						 kolom1=scn.nextInt();
						 System.out.print("Jumlah baris matriks B: ");
						 baris2=scn.nextInt();
						 System.out.print("Jumlah kolom matriks B: ");
						 kolom2=scn.nextInt();
						 break;
					 }catch(InputMismatchException e){
						 System.out.println("Masukkan bilangan bulat");
						 scn.nextLine();
						 continue;
					 }
				 }while(true); 
						if (kolom1 != baris2) {
							System.out.println("Matriks tidak dapat dikalikan");					
							do{
								try{
									System.out.print("Tekan 1 untuk ulangi dan 0 untuk keluar : ");
									choose=scn.nextInt();
									if (choose==0)break;
									if (choose==1)break;
								}catch(InputMismatchException e){
									System.out.println("Masukkan pilihan yang tersedia");
									scn.nextLine();
									continue;
								}
							}while((choose!=0)||(choose!=1));
						}			
						if (choose==0)break;
						if (choose==1)continue;
			 }while(kolom1!=baris2);
				 if (choose==1)break;
				 System.out.println("\n"); 
			int[][]matriks=new int[baris1][kolom1];
			do{
				System.out.println("Matriks A: ");
				for(int indeksbaris=0;indeksbaris<baris1;indeksbaris++){
					for(int indekskolom=0;indekskolom<kolom1;indekskolom++){
						System.out.print("Baris "+(indeksbaris+1)+" Kolom "+(indekskolom+1)+" : ");
						try{
							matriks[indeksbaris][indekskolom]=scn.nextInt();
						}catch(InputMismatchException e){
							System.out.println("Masukkan bilangan bulat");
							scn.nextLine();
							indekskolom--;
							continue;
						}	
					}
				}
				break;
			}while(true);		
			System.out.println();	
			for(int indeksbaris=0;indeksbaris<baris1;indeksbaris++){
				for(int indekskolom=0;indekskolom<kolom1;indekskolom++){
					System.out.printf("%4d ", matriks[indeksbaris][indekskolom]);
				}System.out.println();
			}System.out.println();
			int[][]matriks2=new int[baris2][kolom2];
			do{
				System.out.println("Matriks B: ");
				for(int indeksbaris=0;indeksbaris<baris2;indeksbaris++){
					for(int indekskolom=0;indekskolom<kolom2;indekskolom++){
						System.out.print("row "+(indeksbaris+1)+" Kolom "+(indekskolom+1)+" : ");
						try{
							matriks2[indeksbaris][indekskolom]=scn.nextInt();
						}catch(InputMismatchException e){
							System.out.println("Masukkan bilangan bulat");
							scn.nextLine();
							indekskolom--;
							continue;
						}
					}
				}
				break;
			}while(true);	
			System.out.println();	
			for(int indeksbaris=0;indeksbaris<baris2;indeksbaris++){
				for(int indekskolom=0;indekskolom<kolom2;indekskolom++){
					System.out.printf("%4d ", matriks2[indeksbaris][indekskolom]);
				}System.out.println();
			}	
			int[][]matriks3=new int[baris1][kolom2];
			for(int indeksbaris=0;indeksbaris<baris1;indeksbaris++){
				for(int indekskolom=0;indekskolom<kolom2;indekskolom++){
					matriksMultiTotal=0;
					for(int perkalianindex=0;perkalianindex<kolom1;perkalianindex++){
						matriksMultiTotal+=matriks[indeksbaris][perkalianindex]*matriks2[perkalianindex][indekskolom];
					}
					matriks3[indeksbaris][indekskolom]=matriksMultiTotal;
						}
					}System.out.println();		
			System.out.println("Perkalian dari matriks "+baris1+"x"+kolom1+" X "+baris2+"x"+kolom2+" Akan membuat matriks "+baris1+"x"+kolom2);
			System.out.println("hasil dari perkalian kedua matriks adalah : ");
			for(int indeksbaris=0;indeksbaris<baris1;indeksbaris++){
				for(int indekskolom=0;indekskolom<kolom2;indekskolom++){
					System.out.printf("%4d ", matriks3[indeksbaris][indekskolom]);
				}
				System.out.println();
			}		
			System.out.println("\n");		
			if(baris1 != kolom2){
				System.out.println("Matriks tersebut tidak memiliki determinan");
			}	
			else{
				if(baris1==2 && kolom2==2){
					if(((matriks3[0][0]*matriks3[1][1])-(matriks3[0][1]*matriks3[1][0]))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((matriks3[0][0]*matriks3[1][1])-(matriks3[0][1]*matriks3[1][0])));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
				else if(baris1==3 && kolom2==3){
					if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
						
						System.out.println("Determinan dari matriks tersebut adalah "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
								-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
								+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
			}		
				do{
					try{
						System.out.print("Tekan 1 untuk ulangi dan 0 untuk keluar : ");
						choose=scn.nextInt();
						if (choose==0)break;
						if (choose==1)break;
					}catch(InputMismatchException e){
						System.out.println("Masukkan pilihan yang tersedia");
						scn.nextLine();
						continue;
					}		
				}while((choose<0)||(choose>1));
				if (choose==0)break;
				if(choose==1)continue;
				System.out.println();
			 }
		}
		
		public static void main(String[] args){
				MainMenu();
	    }	
}
