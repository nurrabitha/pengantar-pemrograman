package Tugas2;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
public class no1 {

		private static String inputside;
		private static String inputdiagonal1;
		private static String inputdiagonal2;
		private static String inputpads;
		public static void menuduadimensi() {
		
			System.out.println("two dimension");
			System.out.println("1. triangle");
			System.out.println("2. square");
			System.out.println("3. rectangle");
			System.out.println("4. rhombus");
			System.out.println("5. circle");
			System.out.println("6. trapezoidal");
			System.out.println("7. parallelogram");
			System.out.println("0. out");
		}
		public static void triangleformula (){
			System.out.print("masukkan panjang side ke-satu=");
			BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
			String inputside1=null;
			try{
				inputside1=bufferedreader.readLine();
			}
			catch (IOException error){
				System.out.println("inputan anda salah"+error.getMessage());
		}
			
			System.out.print("masukkan panjang side ke-dua=");
			bufferedreader = new BufferedReader(new InputStreamReader(System.in));
			String inputside2=null;
			try{
				inputside2=bufferedreader.readLine();
			}
			catch (IOException error){
				System.out.println("inputan anda salah"+error.getMessage());

	}
			System.out.print("masukkan panjang sisi ke-tiga=");
			bufferedreader = new BufferedReader(new InputStreamReader(System.in));
			String inputside3=null;
			try{
				inputside3=bufferedreader.readLine();
			}
			catch (IOException error){
				System.out.println("inputan anda salah"+error.getMessage());
				}
			
			try{
				float side1=Float.parseFloat(inputside1);
				float side2=Float.parseFloat(inputside2);
				float side3=Float.parseFloat(inputside3);
				float around = side1+side2+side3;
				double s=0.5*around;
				double area = Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
				System.out.println("keliling segitiga="+around);
				System.out.println("luas segitiga="+area);
			}
			catch(NumberFormatException e){
				System.out.println("inputan anda salah");
			}
		}
			public static void squareformula (){
				System.out.print("masukkan panjang sisi =");
				BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
				String inputside=null;
				try{
					inputside=bufferedreader.readLine();
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());
			}
				
				try{
					float side=Float.parseFloat(inputside);
					double area = Math.pow(side, 2);
					float around=4*side;
					System.out.println("luas persegi adalah="+area);
					System.out.println("keliling persegi adalah="+around);
				}
				catch (NumberFormatException e){
					System.out.println("inputan anda salah");

		}
	}
			public static void rectangleformula (){
				System.out.print("masukkan panjang sisi =");
				BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
				String inputlong=null;
				try{
					inputlong=bufferedreader.readLine();
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());
			}
				System.out.print("masukan lebar persegi panjang=");
				bufferedreader = new BufferedReader(new InputStreamReader(System.in));
				String inputwidth=null;
				try{
					inputwidth = bufferedreader.readLine();
					
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());	}

			try{
				float panjang=Float.parseFloat(inputlong);
				float lebar = Float.parseFloat(inputwidth);
				double panjang1 = panjang*lebar;
			}
			catch (NumberFormatException e){
				System.out.println("inputan anda salah");
			}
	}
			public static void rhombusformula (){
				System.out.print("masukkan panjang diagonal ke-satu =");
				BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
				String inputpandiagonal1=null;
				try{
					inputdiagonal1 = bufferedreader.readLine();
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());
			}
				System.out.print("masukan panjang diagonal ke-dua=");
				bufferedreader = new BufferedReader(new InputStreamReader(System.in));
				String inputdiagonal2=null;
				try{
					inputdiagonal2 = bufferedreader.readLine();
					
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());	}

			try{
				
				float diagonal1=Float.parseFloat(inputdiagonal1);
				float diagonal2 = Float.parseFloat(inputdiagonal2);
				double area = diagonal1*diagonal2;
				double side = Math.sqrt(Math.pow(0.5*diagonal1, 2)+Math.pow(0.5*diagonal2, 2));
				double keliling = 4*side;
				System.out.println("luas belah ketupat ="+area);
				System.out.println("keliling belah ketupat="+keliling);
			}
			catch (NumberFormatException e){
				System.out.println("inputan anda salah");
			}
	}
			public static void circleformula (){
				System.out.print("masukkan panjang jari-jari lingkaran =");
				BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
				String inputdiameters=null;
				try{
					inputdiameters = bufferedreader.readLine();
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());
			}
				
				try{
					float diameters = Float.parseFloat(inputdiameters);
					double keliling = 2*Math.PI*diameters;
					double area = Math.PI*Math.pow(diameters,2 );
					System.out.println("luas lingkaran adalah="+area);
					System.out.println("keliling lingkaran adalah="+keliling);
					
				}
				catch (NumberFormatException e){
					System.out.println("inputan anda salah");	
					}
	}
			public static void trapezoidalformula () throws IOException{
				System.out.print("masukkan sisi ke-satu trapesium =");
				BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
				String inputside1=null;
				try{
					inputside1 = bufferedreader.readLine();
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());
			}
				System.out.print("masukkan sisi ke-dua trapesium  =");
				bufferedreader = new BufferedReader(new InputStreamReader(System.in));
				String inputside2=null;
				try{
					inputside2= bufferedreader.readLine();
					
				}
				catch (NumberFormatException e){
					System.out.println("inputan anda salah");	
					}
				System.out.print("masukkan sisi ke-tiga trapesium =");
				bufferedreader = new BufferedReader(new InputStreamReader(System.in));
				String inputside3=null;
				try{
					inputside3 = bufferedreader.readLine();
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());
	}
				System.out.print("masukkan sisi ke-empat trapesium =");
				bufferedreader = new BufferedReader(new InputStreamReader(System.in));
				String inputside4=null;
				try{
					inputside4 = bufferedreader.readLine();
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());
	}
				System.out.print("masukkan high trapesium =");
				bufferedreader = new BufferedReader(new InputStreamReader(System.in));
				String inputhigh=null;
				try{
					inputhigh = bufferedreader.readLine();
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());
	}
				try{
					float side1 = Float.parseFloat(inputside1);
					float side2 = Float.parseFloat(inputside2);
					float side3 = Float.parseFloat(inputside3);
					float side4 = Float.parseFloat(inputside4);
					float high = Float.parseFloat(inputhigh);
					double area = 0.5*(side1+side2)*high;
					double keliling = side1+side2+side3+side4;
					System.out.println("luas trapesium adalah="+area);
					System.out.println("keliling trapesium adalah="+keliling);
			}
				catch(NumberFormatException error){
					System.out.println("inputan anda salah");
				}
	}
			public static void parallelogramformula (){
				System.out.print("masukkan panjang alas jajar genjang =");
				BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
				String inputpads=null;
				try{
					inputpads = bufferedreader.readLine();
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());
			}
				System.out.print("masukkan panjang side miring jajar genjang");
				bufferedreader = new BufferedReader (new InputStreamReader(System.in));
				String inputmiring=null;
				
				try{
					inputmiring = bufferedreader.readLine();
					
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());	
					}
				System.out.print("masukkan panjang side high jajar genjang");
				bufferedreader = new BufferedReader (new InputStreamReader(System.in));
				String inputhigh=null;
				
				try{
					inputhigh = bufferedreader.readLine();
					
				}
				catch (IOException error){
					System.out.println("inputan anda salah"+error.getMessage());	
					}
				try{
					float alas=Float.parseFloat(inputpads);
					float miring=Float.parseFloat(inputmiring);
					float high=Float.parseFloat(inputhigh);
					double area = 0.5*high*alas;
					double keliling = 2*(alas+miring);
					System.out.println("luas jajar genjang adalah="+area);
					System.out.println("keliling jajar genjang adalah="+keliling);
				}
				catch(NumberFormatException e){
					System.out.println("inputan anda salah");
				}
	}
				public static void menutigadimensi(){
					System.out.println("jenis-jenis bangun ruang dua dimensi ");
					System.out.println("1. kubus");
					System.out.println("2. balok");
					System.out.println("3. limas segiempat");
					System.out.println("4. prisma segitiga");
					System.out.println("5. kerucut");
					System.out.println("6. tabung");
					System.out.println("0. keluar");
				}
				public static void cubeformula(){
					System.out.print("masukkan panjang side kubus =");
					BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
					String inputside=null;
					try{
						inputside = bufferedreader.readLine();
					}
					catch (IOException error){
						System.out.println("inputan anda salah"+error.getMessage());
				}	
					try{
						float side = Float.parseFloat(inputside);
						double luas = 6*Math.pow(side, 2);
						double keliling = 12*side;
						double volume = Math.pow(side, 3);
						System.out.println("luas permukaan kubus adalah="+luas);
						System.out.println("keliling permukaan kubus adalah=");
						System.out.println("volume kubus adalah=");
						
					}
					catch (NumberFormatException error){
						System.out.println("inputan anda salah");	
		}
			}
				public static void beamformula(){
					System.out.print("masukkan panjang balok adalah =");
					BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
					String inputlong=null;
					try{
						inputlong = bufferedreader.readLine();
					}
					catch (IOException error){
						System.out.println("inputan anda salah"+error.getMessage());
				}
					System.out.print("masukkan lebar balok adalah =");
					bufferedreader = new BufferedReader(new InputStreamReader(System.in));
					String inputlebar=null;
					try{
						inputlebar = bufferedreader.readLine();
					}
					catch (IOException error){
						System.out.println("inputan anda salah"+error.getMessage());
					}
					System.out.print("masukkan high balok adalah =");
					bufferedreader = new BufferedReader(new InputStreamReader(System.in));
					String inputhigh=null;
					try{
						inputhigh = bufferedreader.readLine();
					}
					catch (IOException error){
						System.out.println("inputan anda salah"+error.getMessage());
					}
					try{
						float panjang = Float.parseFloat(inputlong);
						float lebar = Float.parseFloat(inputlebar);
						float high = Float.parseFloat(inputhigh);
						double area = 2*((panjang*lebar)+(panjang*high)+(lebar*high));
						double keliling = 4*(panjang+lebar+high);
						double volume = panjang*lebar*high;
						System.out.println("luas permukaan kubus adalah="+area);
						System.out.println("keliling permukaan kubus adalah=");
						System.out.println("volume kubus adalah=");
						
					}
					catch(NumberFormatException e){
						System.out.println("inputan anda salah");
					}
				}
					public static void rectangularpyramidformula(){
						System.out.print("masukkan panjang side alas adalah =");
						BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
						String inputside=null;
						try{
							inputside = bufferedreader.readLine();
						}
						catch (IOException error){
							System.out.println("inputan anda salah"+error.getMessage());
					}
						System.out.print("masukkan high limas adalah =");
						bufferedreader = new BufferedReader(new InputStreamReader(System.in));
						String inputhigh=null;
						try{
							inputhigh = bufferedreader.readLine();
						}
						catch (IOException error){
							System.out.println("inputan anda salah"+error.getMessage());
						}
						try{
							float side = Float.parseFloat(inputside);
							float high = Float.parseFloat(inputhigh);
							double padsarea = Math.pow(side, 2);
							double highside = Math.sqrt(Math.pow(0.5*side,2)*Math.pow(high, 2));
							double areaside = 0.5*side*highside;
							double area = padsarea+(4*areaside);
							double keliling = (4*side)+(4*Math.sqrt(Math.pow(highside,2)+Math.pow(0.5*side,2)));
							double volume =1/3*padsarea*high ;
							System.out.println("luas limas segiempat adalah="+area);
							System.out.println("keliling limas segiempat adalah="+keliling);
							System.out.println("volume limas segiempat adalah="+volume);
						}
						catch(NumberFormatException e){
							System.out.println("inputan anda salah");
						}
					}
					public static void triangularprismformula(){
						System.out.print("masukkan panjang side alas ke-1 adalah =");
						BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
						String inputside1=null;
						try{
							inputside1 = bufferedreader.readLine();
						}
						catch (IOException error){
							System.out.println("inputan anda salah"+error.getMessage());
					}
						System.out.print("masukkan panjang side alas ke-2 adalah =");
						bufferedreader = new BufferedReader(new InputStreamReader(System.in));
						String inputside2=null;
						try{
							inputside2 = bufferedreader.readLine();
						}
						catch (IOException error){
							System.out.println("inputan anda salah"+error.getMessage());
						}
						System.out.print("masukkan panjang side alas ke-3 adalah =");
						bufferedreader = new BufferedReader(new InputStreamReader(System.in));
						String inputside3=null;
						try{
							inputside3 = bufferedreader.readLine();
						}
						catch (IOException error){
							System.out.println("inputan anda salah"+error.getMessage());
						}
						System.out.print("masukkan high prisma adalah =");
						bufferedreader = new BufferedReader(new InputStreamReader(System.in));
						String inputhigh=null;
						try{
							inputhigh = bufferedreader.readLine();
						}
						catch (IOException error){
							System.out.println("inputan anda salah"+error.getMessage());
						}
						try{
							float side1 = Float.parseFloat(inputside1);
							float side2 = Float.parseFloat(inputside2);
							float side3 = Float.parseFloat(inputside3);
							float high = Float.parseFloat(inputhigh);
							float keliling= 2*(side2+side2+side3)+3*high;
							double s = 0.5*keliling;
							double triangleprismarea = Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
							double area = (side1+side2+side3)*high*2*triangleprismarea;
							double volume = triangleprismarea*high;
							System.out.println("keliling prisma segitiga adalah = "+keliling);
							System.out.println("luas prisma segitiga adalah = "+area);
							System.out.println("volume prisma segitiga adalah = "+volume);
						}
						catch(NumberFormatException e){
							System.out.println("inputan anda salah");
						}
					}
					public static void coneformula(){
						System.out.print("masukkan jari-jari adalah =");
						BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
						String inputdiameters=null;
						try{
							inputdiameters = bufferedreader.readLine();
						}
						catch (IOException error){
							System.out.println("inputan anda salah"+error.getMessage());
					}
						System.out.print("masukkan high kerucut adalah =");
						bufferedreader = new BufferedReader(new InputStreamReader(System.in));
						String inputhigh=null;
						try{
							inputhigh = bufferedreader.readLine();
						}
						catch (IOException error){
							System.out.println("inputan anda salah"+error.getMessage());
						}
						try{
							float diameters = Float.parseFloat(inputdiameters);
							float high = Float.parseFloat(inputhigh);
							double area = Math.PI*diameters*(diameters+Math.sqrt(Math.pow(high, 2)+Math.pow(diameters, 2)));
							double volume = 1/3*Math.PI*Math.pow(diameters, 2)*high;
							System.out.println("luas kerucut adalah = "+area);
							System.out.println("Volume kerucut adalah ="+volume);
						}
						catch(NumberFormatException e){
							System.out.println("inputan anda salah");
						}
					}
					public static void tubeformula(){
						System.out.print("masukkan jari-jari adalah =");
						BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
						String inputdiameters=null;
						try{
							inputdiameters = bufferedreader.readLine();
						}
						catch (IOException error){
							System.out.println("inputan anda salah"+error.getMessage());
					}
						System.out.print("masukkan high tabung adalah =");
						bufferedreader = new BufferedReader(new InputStreamReader(System.in));
						String inputhigh=null;
						try{
							inputhigh = bufferedreader.readLine();
						}
						catch (IOException error){
							System.out.println("inputan anda salah"+error.getMessage());
						}
						try{
							float diameters = Float.parseFloat(inputdiameters);
							float high = Float.parseFloat(inputhigh);
							double area = 2*Math.PI*diameters*(diameters+high);
							double volume = Math.PI*Math.pow(diameters, 2)*high;
							System.out.println("luas tabung adalah = "+area);
							System.out.println("Volume tabung adalah ="+volume);
						}
						catch(NumberFormatException e){
							System.out.println("inputan anda salah");
						}
					}
					public static void main(String[] args){
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
						String inputdata=null;
						int pilih =0;
						int pilih1 =0;
						do{
							System.out.println("\nRumus-rumus bangun ruang dua dimensi dan tiga dimensi");
							System.out.println("1. Rumus Bangun ruang dua dimensi");
							System.out.println("2. Rumus Bangun ruang tiga dimensi");
							System.out.println("\nMasukkan pilihan anda = ");
							try{
								inputdata = bufferedReader.readLine();
								try{
									pilih = Integer.parseInt(inputdata);
									if(pilih >0 && pilih ==1){
										menuduadimensi();
										try{
											inputdata = bufferedReader.readLine();
											try{
											if (pilih1 >0 && pilih1 ==1){
												triangleformula();
											}
											else if (pilih1 > 0 && pilih1==2){
												squareformula();
											}
											else if(pilih1 > 0 && pilih1==3){
												rectangleformula();
											}
											else if(pilih1 >0 && pilih1==4){
												rhombusformula();
											}
											else if(pilih1 >0 && pilih1==5){
												circleformula();
											}
											else if(pilih1 >0 && pilih1==6){
												trapezoidalformula();
											}
											else if(pilih1 >0 && pilih1==7){
												parallelogramformula();
											}
											else if(pilih1 == 0 ){
												break;
											}
											else{
												System.out.println("masukkan anda salah");
											}
										}
									catch(NumberFormatException e){
										System.out.println("inputan anda salah");
										}
									}
									catch(IOException error){
										System.out.println("inputan anda salah"+error.getMessage());
									}
								}	
									else if(pilih >0 && pilih ==2){
										menutigadimensi();
										try{
											System.out.println("\nMasukkan pilihan anda");
											inputdata = bufferedReader.readLine();
											try{
												int pilih2 = Integer.parseInt(inputdata);
												if (pilih2 > 0 && pilih2 ==1){
													cubeformula();
												}
												else if (pilih2 > 0 && pilih2 ==2){
													beamformula();
												}
												else if (pilih2 > 0 && pilih2 ==3){
													rectangularpyramidformula();
												}
												else if (pilih2 > 0 && pilih2 ==4){
													triangularprismformula();
												}
												else if (pilih2 > 0 && pilih2 ==5){
													coneformula();
												}
												else if (pilih2 > 0 && pilih2 ==6){
													tubeformula();
												}
												else if(pilih2 ==0 ){
													break;
												}
												else{
													System.out.println("masukkan anda salah");
												}
											}
											catch(NumberFormatException e){
												System.out.println("inputan anda salah");
											}
										}
										catch(IOException error){
											System.out.println("inputan anda salah"+error.getMessage());
										}
									}
								}
								catch(NumberFormatException e){
									System.out.println("inputan anda salah");
								}
							}
							catch(IOException error){
								System.out.println("inputan anda salah"+error.getMessage());
							}
							System.out.println("masukkan pilihan anda");
						}
						while(pilih>0);
					}
				}
